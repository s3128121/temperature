package nl.utwente.di.bookQuote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the Quoter
 */
public class TestConversion {
    @Test
    public void testBook1( ) throws Exception {
        Conversion conversion = new Conversion();
        double price = conversion.getBookPrice("1") ;
        Assertions.assertEquals (10.0 , price , 0.0 , "Price of book 1") ;
    }
}

