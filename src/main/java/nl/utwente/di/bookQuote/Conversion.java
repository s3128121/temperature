package nl.utwente.di.bookQuote;

public class Conversion {

    public Conversion() {
    }

    public double getBookPrice(String temperature) {
        double realTemp = Integer.parseInt(temperature);
        return (realTemp * ((double) 9 /5)) + 32;
    }

}
